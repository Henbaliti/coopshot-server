const mongoose = require("mongoose");
require("./AlbumPhoto");

const albumSchema = new mongoose.Schema({
  name: {
    type: String,
    default: "No Name",
  },
  description: {
    type: String,
    default: "",
  },
  partners: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: false,
    },
  ],
  photos: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Photo",
      required: false,
    },
  ],
});

module.exports = mongoose.model("Album", albumSchema);
