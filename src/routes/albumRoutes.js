const express = require('express');
const mongoose = require('mongoose');
const requireAuth = require('../middlewares/requireAuth');

const Album = mongoose.model('Album');

const router = express.Router();

router.use(requireAuth);

router.get('/albums', async (req, res) => {
  const albums = await Album.find({ ownUserId: req.user._id });
  //Todo - Add the albums which are not in user`s own, but partner

  res.send(albums);
});

router.post('/albums', async (req, res) => {
  const { name, partners} = req.body;

  if (!name) {
    return res
      .status(422)
      .send({ error: 'You must provide a name' });
  }

  try {
    const album = new Album({ name, partners});
    await album.save();
    res.send(album);
  } catch (err) {
    res.status(422).send({ error: err.message });
  }
});

router.put('/albums', async (req, res) => {
    // Implement this method for invite people to album
    // Consider to seperate the addphotos and addpartners
  });

module.exports = router;
