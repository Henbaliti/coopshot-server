const { cloudinary } = require("../utils/cloudinary");
const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const Album = mongoose.model("Album");
const Photo = mongoose.model("Photo");
const User = mongoose.model("User");
const upload = require('../utils/multer');
const bodyParser = require("body-parser");
var cors = require("cors");

router.use(bodyParser.urlencoded({ extended: true }));
router.use(express.static("public"));
router.use(express.json({ limit: "50mb" }));
router.use(express.urlencoded({ limit: "50mb", extended: true }));
router.use(cors());

// router.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });

router.get("/api/images", async (req, res) => {
  //Not tested yet
  const { resources } = await cloudinary.search
    .expression("folder:dev_setups")
    .sort_by("public_id", "desc")
    .max_results(30)
    .execute();

  const publicIds = resources.map((file) => file.public_id);
  res.send(publicIds);
});

//method to upload single or several images
router.post("/api/upload", upload.array("photos",12), async (req, res) => {
  try {

    //Get the request data
    const {
      albumPath, //e.g userName~albumName
      private,
      lat,
      long,
      alt,
      timestamp,
    } = req.body;

    //Get the photos as files
    const photos = req.files;

    /*-----------Generate the required Photo scheme data-------*/

    //Get the userID from the album path
    const userID = albumPath.split("~")[0];

    const coords = {
      latitude: lat,
      longitude: long,
      altitude: alt,
    };

    //Get the user
    author = await User.findOne({ _id: userID });
    if (!author) {
      //Send error msg
      res.status(400).json({
        message: "User not found",
      });
      return;
    }

    var album = await Album.findOne({ name: albumPath });
    if (!album) {
      //Send error msg
      res.status(400).send({
        message: "Album not found",
      });
      return;
    }

    //Upload all photos to cloud
    responses = [];
    for (const file of photos) {
      const newPath = await cloudinary.uploader.upload(file.path, {
        folder: albumPath,
      });
      responses.push(newPath);
    }
    
    //Save in mongo
    responses.forEach((response) => {
      const url = response.url;
      const public_id = response.public_id;
      const photo = new Photo({
        url,
        public_id,
        album,
        author,
        private,
        timestamp,
        coords,
      });
      photo.save();
      album.photos.push(photo);
      album.save();
    });

    //Return a msg to client that uploaded - unnecessary for production version
    res.status(200).json({
      message: "images uploaded successfully",
      //data: responses,
    });
  } catch (err) {
    console.error(err);
    res.status(500).json({ err: "Something went wrong" });
  }
});

router.post("/api/deletephoto", async (req, res) => {
  //Not tested yet - need to store the public id in the db first
  const image_id = req.body.public_id;
  const response = await cloudinary.uploader.destroy(image_id);
  if (response === "not found") {
    return res.status(400).json("Image not found");
  }
  return res.status(200).json("Delete Image successfuly");
});

router.post("/api/deleteAlbum", async (req, res) => {
  const albumName = req.body.albumName;
  //Delete all the images in the album by prefix
  cloudinary.api.delete_resources_by_prefix(`${albumName}/`, function (err) {
    if (err && err.http_code !== 404) {
      return res.status(400).json("error while deleting images");
    }

    //Delete the folder it self
    cloudinary.api.delete_folder(`${albumName}`, function (error, result) {
      if (error && error.http_code !== 404) {
        return res.status(400).json("error when delete album");
      }
      return res.status(200).json("Delete album succesfuly");
    });
  });
});

router.post("/api/createalbum", async (req, res) => {
  try {
    //NOTE - there is no method for simply create empty folder in cloudinary,
    //the most close to is to upload file to un-existing folder and then cloudinary generate the folder.
    //The solution - post a request to upload default photo to un-existing folder and then delete the photo.
    //The folders has to be unique so lets generate the name like so - `${user identifer}${album name}`

    console.log('request arrived')
    const { userID, albumName, partners, description } = req.body;

    const combinedName = `${userID}~${albumName}`;

    if (!albumName || !userID) {
      return res.status(422).send({ error: "You must provide a name" });
    }

    const myDefaultIMG = "../src/images/defaultIMG.png";
    const uploadResponse = await cloudinary.uploader.upload(myDefaultIMG, {
      folder: combinedName,
    });

    //Delete the default image
    await cloudinary.uploader.destroy(uploadResponse.public_id);

    const album = new Album({ name: combinedName, description, partners });
    await album.save();
    res.send(album);
  } catch (err) {
    console.error(err);
    res.status(500).send({ err: "Something went wrong" });
  }
});

module.exports = router;
